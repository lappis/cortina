import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import { Card } from 'react-native-material-design';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AttributeCard extends Component {
  render() {
    return (
      <Card style={ styles.card } elevation={ 5 }>
        <Card.Body>
          <View style={ styles.cardHeader }>
            <Text style={ styles.cardTitle }>{ this.props.attr.name }</Text>
            <View style={ styles.cardImgWrapper }>
              <Icon name={ this.props.attr.icon } style={ styles.cardImg }/>
            </View>
          </View>
          <View style={ styles.cardActions }>
            <Icon.Button name="times-circle" backgroundColor='#D32F2F'>Não</Icon.Button>
            <Icon.Button name="check-circle" backgroundColor='#388E3C'>Sim</Icon.Button>
          </View>
        </Card.Body>
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    width: 330,
    height: 300,
    paddingTop: 10
  },
  cardHeader: {
    alignItems: 'center'
  },
  cardTitle: {
    fontSize: 24
  },
  cardImgWrapper: {
    marginTop: 25,
    padding: 20,
    backgroundColor: '#0288d1',
    borderRadius: 100,
    width: 100,
    height: 100
  },
  cardImg: {
    backgroundColor: 'transparent',
    fontSize: 60,
    color: 'white'
  },
  cardActions: {
    marginTop: 40,
    justifyContent: 'space-around',
    flexDirection: 'row'
  }
})

import React, { Component } from 'react';
import { ListView } from 'react-native';

import AttributeCard from './attribute_card'

export default class AttributesCardList extends Component {
  constructor(props) {
    super(props);

    // Just for testing. The school should be a prop.
    let school = {
      name: 'Escola Feliz',
      attributes: [
        {name: 'Acesso para Deficientes', icon: 'wheelchair'},
        {name: 'Acesso à Internet', icon: 'wifi'},
        {name: 'Quadra de Esportes', icon: 'soccer-ball-o'},
      ]
    }

    this.state = {
      school: school,
      attrsDataSource: this.getAttrsDataSource([])
    };
  }

  componentDidMount() {
    let dataSource = this.getAttrsDataSource(this.state.school.attributes)

    this.setState({
      attrsDataSource: dataSource
    });
  }

  getAttrsDataSource(attrs) {
    let dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => true
    });
    return dataSource.cloneWithRows(attrs);
  }

  render() {
    return (
      <ListView
        horizontal={ true }
        dataSource={ this.state.attrsDataSource }
        renderRow={ (itemData) => {
          return <AttributeCard attr={ itemData } />
        }}
      />
    )
  }
}

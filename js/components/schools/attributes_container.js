import React, { Component } from 'react';
import {
  View,
  Modal,
  ListView,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Card } from 'react-native-material-design';

import AttributeModal from './attribute_modal.js'

export default class AttributesContainer extends Component {
  constructor(props) {
    super(props);

    // Just for testing. The school should be a prop.
    let school = {
      name: 'Escola Feliz',
      attributes: [
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: false},
        {name: 'Quadra de Esportes', icon: 'soccer-ball-o', exists: true},
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: true},
        {name: 'Quadra de Esportes', icon: 'soccer-ball-o', exists: true},
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: true},
        {name: 'Quadra de Esportes', icon: 'soccer-ball-o', exists: false},
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: false},
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: false},
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: false},
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: false},
        {name: 'Acesso para Deficientes', icon: 'wheelchair', exists: true},
        {name: 'Acesso à Internet', icon: 'wifi', exists: false},
      ]
    }

    this.state = {
      school: school,
      selectedAttr: {name: '', icon: '', exists: true},
      modalVisible: false,
      attrsDataSource: this.getAttrsDataSource([])
    };
  }

  componentDidMount() {
    let dataSource = this.getAttrsDataSource(this.state.school.attributes)

    this.setState({
      attrsDataSource: dataSource
    });
  }

  getAttrsDataSource(attrs) {
    let dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => true
    });
    return dataSource.cloneWithRows(attrs);
  }

  renderAttrItem(attr) {
    return(
      <View style={ styles.iconWrapper }>
        <TouchableOpacity
          onPress={ this.displayAttrInfo.bind(this, attr) }
          style={ styles.touchableOpacity }>

          <Icon
            size={ 30 }
            style={ styles.icon }
            name={ attr.icon }
            backgroundColor='transparent'
          />
        </TouchableOpacity>
      </View>
    );
  }

  displayAttrInfo(attr) {
    this.setState({
      selectedAttr: attr,
      modalVisible: true
    });
  }

  closeModal() {
    this.setState({ modalVisible: false });
  }

  render() {
    return(
      <View>
      <AttributeModal
        closeModal={ this.closeModal.bind(this) }
        attr={ this.state.selectedAttr }
        visible={ this.state.modalVisible } />

          <ListView
            style={ styles.listContainer }
            contentContainerStyle={ styles.list }
            dataSource={ this.state.attrsDataSource }
            initialListSize={ 40 }
            keyboardShouldPersistTaps={true}
            renderRow={ (itemData) => this.renderAttrItem(itemData) } />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    height: 200,
    backgroundColor: 'transparent'

  },
  listContainer: {
    marginTop: -16,
    marginBottom: -16
  },
  list: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  iconWrapper: {
    margin: 10,
    marginBottom: 20,
    backgroundColor: '#0288d1',
    borderRadius: 100,
    width: 60,
    height: 60
  },
  touchableOpacity: {
    backgroundColor: 'transparent'
  },
  icon: {
    color: 'white',
    borderRadius: 100,
    padding: 15,
    width: 60,
    height: 60
  }
})

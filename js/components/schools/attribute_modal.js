import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Slider,
  Modal
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AttributeModal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let status = this.props.attr.exists ? 'TEM' : 'NÃO TEM';

    return (
      <Modal
        visible={ this.props.visible }
        animationType={ 'fade' }
        onRequestClose={ this.props.closeModal }
        transparent={ true }>

        <View style={ styles.modalContainer }>

          <View style={ styles.innerContainer }>
            <View style={ styles.attrHeader }>
              <Text style={ styles.attrTitle }>
                { this.props.attr.name }
              </Text>

              <Icon
                size={ 30 }
                style={ styles.icon }
                name={ this.props.attr.icon }
                backgroundColor='transparent'/>
            </View>

            <Text style={ styles.attrBody }>
              Oficialmente consta que esta escola {status}.
            </Text>
            <Text style={ styles.attrBody }>
              Já segundo o relato popular...
            </Text>

            <Slider
              style={ styles.slider }
              disabled={ true }
              value={ 30 }
              maximumValue={ 100 }
              minimumTrackTintColor={'red'}
              maximumTrackTintColor={'green'} />

            <View style={ styles.sliderCaption }>
              <Text>Não tem</Text>
              <Text>Tem</Text>
            </View>

            <TouchableOpacity onPress={ this.props.closeModal }>
              <Text style={ styles.closeOption }>Fechar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  innerContainer: {
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 10,
    width: 300
  },
  attrHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15
  },
  attrTitle: {
    fontSize: 20
  },
  attrBody: {
  },
  slider: {
    marginTop: 10
  },
  sliderCaption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  closeOption: {
    marginTop: 20,
    textAlign: 'center',
    color: '#0288d1'
  }
});

import React, { Component } from 'react';
import {
  View,
  Text,
  Alert,
  ListView,
  StyleSheet,
  TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import { Card } from 'react-native-material-design';

import TouchableListItem from './shared/TouchableListItem';

export default class ListSchools extends Component {
  constructor() {
    super();

    this.state = {
      schoolsDataSource: this.getSchoolsDataSource([])
    };
  }

  componentDidMount() {
    let dataSource = this.getSchoolsDataSource(this.props.schools)

     this.setState({
        schoolsDataSource: dataSource
    });
  }

  getSchoolsDataSource(schools) {
      let dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.code !== r2.code});
      return dataSource.cloneWithRows(schools);
  }

  handleSchoolOnPress(school) {
      console.log(school); // use react-native log-android to see this log
  }

  renderSchool(school) {
    return (
      <Card>
        <Card.Body>
          <View style={ styles.cardIconContainer }>
            <View>
              <Text style={ styles.cardTitle }>{ school.name }</Text>
              <Text style={ styles.cardInfo }>Endereço: { school.code }</Text>
              <Text style={ styles.cardInfo }>Telefone: { school.code }</Text>
            </View>
            <View style={{ justifyContent: 'flex-end' }}>
            <Icon.Button
              name='map-marker'
              color='green'
              style={ styles.cardIcon }>
              <Text style={{ color: 'green' }}>{ school.code }</Text>
            </Icon.Button>
            </View>
          </View>
        </Card.Body>
      </Card>
    );
  }

  render() {
    return (
      <ListView
        dataSource={this.state.schoolsDataSource}
        renderRow={(rowData) => this.renderSchool(rowData)}
      />
    );
  }
}

const styles = StyleSheet.create({
  cardTitle: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  cardInfo: {
    fontSize: 15,
    color: '#b3b3b3'
  },
  listHead: {
    backgroundColor: 'white',
    padding: 5,
    marginTop: 10
  },
  headText: {
    color: 'grey',
    fontSize: 20
  },
  cardIconContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  cardIcon: {
    flex: 1,
    alignSelf: 'flex-end',
    backgroundColor: 'white',
    borderRadius: 0
  }
});

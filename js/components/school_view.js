import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView
} from 'react-native';

import AttributesContainer from './schools/attributes_container';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class SchoolView extends Component {
  constructor(props) {
    super(props);
  }
  render() {

    return (
      <View>
      <ScrollView>
        <View>
          <Image source={{uri: "https://pixabay.com/static/uploads/photo/2016/08/24/16/20/books-1617327_1280.jpg"}} style={styles.imageContainer}>
          </Image>
        </View>
          <View style={styles.container}>
          <View>
            <Text style={styles.schoolTitle}>Nome Escola</Text>
          </View>
          <View style={styles.boxLine}>
          </View>
          <View style={styles.buttonStyle}>
            <Icon.Button name='check-square-o' backgroundColor='#1565C0' borderRadius={10} style={styles.buttonHeight} >
            <Text style={styles.buttonTextStyle}>
              Avalie esta Escola
            </Text>
          </Icon.Button>
          </View>
          <View style={styles.attributesView}>
            <Text style={styles.schoolInfo}>Nome Escola</Text>
            <Text style={styles.schoolInfo}>Telefone</Text>
            <Text style={styles.schoolInfo}>Endereço</Text>
            </View>
        </View>
        <AttributesContainer/>
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  schoolTitle: {
    fontSize: 30,
    paddingTop: 0,
  },
  imageContainer: {
    height: 150,
    justifyContent: 'flex-end',
    shadowOffset: { height:1, width: 0 }
  },
  boxLine: {
    borderColor: 'lightgrey',
    borderStyle: 'solid',
    borderTopWidth: 1,
    margin: 5
  },
  schoolInfo: {
    fontSize: 15,
    paddingTop: 10,
  },
  container: {
    padding: 10
  },
  attributesView:{
    marginBottom: 50
  },
  buttonStyle: {
    padding:10,
  },
  buttonHeight: {
    height: 50,
  },
  buttonTextStyle: {
    fontSize: 20,
    color: 'white',
    textAlign: 'right'
  }
});

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

export default class NotFound extends Component {
  // TODO: Add a funny image for the 404 page
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.message}>Ops, não pode carregar o conteudo requisitado</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },

  message: {
    fontSize: 14,
    fontWeight: "600"
  }
});

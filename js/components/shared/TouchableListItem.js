import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight
} from 'react-native';

export default class TouchableListItem extends Component {
  render() {
    return (
      <TouchableHighlight onPress={() => this.props.onPressCallback(this.props.data)}>
        <View style={styles.container}>
          {this.props.children}
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 10,
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#000000',
    backgroundColor: '#ffffff'
  }
});

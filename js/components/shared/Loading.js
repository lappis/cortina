import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator
} from 'react-native';

class Loading extends Component {
  render() {
    return (
      <View style={styles.loadContainer}>
        <Text style={styles.loadingMessaage}>{this.props.message}</Text>

        <ActivityIndicator
          animating={true}
          style={styles.loader}
          size="large"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loadContainer: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },

  loadingMessaage: {
    fontSize: 25,
    fontWeight: "600",
    textAlign: "center",
  },

  loader: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
    height: 80
  }
});

export default Loading;

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text
} from 'react-native';

import Config from '../environment';
import axios from 'axios';
import Search from './search';
import ListSchools from './ListSchools';
import AttributesCardList from './schools/attributes_card_list';
import AttributesContainer from './schools/attributes_container';
import Loading from './shared/Loading';
import SchoolView from './school_view';

export default class Base extends Component {
  constructor() {
    super();

    axios.defaults.baseURL = Config.baseUrl;
  }

  render() {

    return (
      // To display cards list
      // <AttributesCardList />

      // To display the attributes list
      <SchoolView />
    );
  }
}

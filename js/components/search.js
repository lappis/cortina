import React, { Component } from 'react';
import {
    View,
    TextInput
} from 'react-native';
import Permissions from 'react-native-permissions';

import Config from '../environment';
import axios from 'axios';

import SearchBar from './search_bar'
import ListSchools from './ListSchools';
import Loading from './shared/Loading';
import NotFound from './shared/not_found';

export default class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      schools: [],
      useLocationHighAccuracy: true,
      schoolsLoadState: 0
    };
  }

  componentDidMount() {
    this.getUserLocation();
  }

  getUserLocation() {
    Permissions.requestPermission('location')
      .then(() => {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({position});
            this.fetchSchoolsData();
          },
          (error) => {
            // code -> 3, time out. Then try with low accurary
            if (error.code === 3 && this.state.useLocationHighAccuracy) {
              this.setState({
                useLocationHighAccuracy: false
              });

              this.getUserLocation();
            }
          },
          {enableHighAccuracy: this.state.useLocationHighAccuracy, timeout: 5000}
        );
      });
  }

  generateEndpoint(query) {
    let latitude = this.state.position.coords.latitude;
    let longitude = this.state.position.coords.longitude;
    let radius = 10; // TODO: move radius parameter to app config

    return '/rest/escolas/latitude/'+latitude+'/longitude/'+longitude+'/raio/'+radius;
  }

  fetchSchoolsData(query, page, schoolList) {
    query = query || '';
    page = page || 0;
    schoolList = schoolList || [];

    axios.get(this.generateEndpoint(), {
      params: {
        pagina: page,
        quantidadeDeItens: 10,
        campos: "nome,codEscola"
      }
    })
    .then((response) => {
      let regex = new RegExp(query, 'ig');
      let data = response.data
        .map((school) => {
          return {
            code: school.codEscola,
            name: school.nome
          }
        })
        .filter((school) => {
          return school.name.match(regex);
        });

      schoolList = schoolList.concat(data);

      // TODO: Move perPage to a config file
      if(schoolList.length >= 10 || response.data.length == 0){
        this.setState({
          schoolsLoadState: 1,
          schools: schoolList
        });
      } else {
        console.log(schoolList.length);
        this.fetchSchoolsData(query, page + 1, schoolList);
      }
    })
    .catch((err) => {
      this.setState({
        schoolsLoadState: 2
      });
    });
  }

  handleSearchInput(query) {
    this.setState({schoolsLoadState: 0});
    this.fetchSchoolsData(query);
  }

  render() {
    let content = "";

    /*
      schoolsLoadState:
        0 -> Getting user position and loading schools
        1 -> Schools loaded, displaying shool list
        2 -> Network error, could not get schools, display a error message to user
    */
    switch(this.state.schoolsLoadState) {
      case 0:
        content = <Loading />;
        break;

      case 1:
        content = <ListSchools schools={this.state.schools} />;
        break;

      case 2:
        content = <NotFound />
        break;
    }

    console.log('Load state: ' +  this.state.schoolsLoadState);

    return (
      <View style={{flex: 1}}>
        <SearchBar onSearchClick={this.handleSearchInput.bind(this)} />
        {content}
      </View>
    );
  }
}

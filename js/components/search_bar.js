import React, { Component } from 'react';
import {
    View,
    TextInput,
    Image,
    StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Config from '../environment';
import axios from 'axios';

export default class SearchBar extends Component {
    constructor(props) {
      super(props);
      this.state = { query: '' };
    }

    render() {
        return (
            <Image source={{uri: "https://pixabay.com/static/uploads/photo/2016/08/24/16/20/books-1617327_1280.jpg"}} style={styles.imageContainer}>
                <View style={styles.container}>
                  <TextInput placeholder='Procure por uma escola'
                             placeholderTextColor='#cacaca'
                             underlineColorAndroid='#0288d1'
                             style={styles.searchInput}
                             onChangeText={(query) => this.setState({query})}
                             onSubmitEditing={() => this.props.onSearchClick(this.state.query)} />
                  <Icon.Button name='search' backgroundColor='#0288d1'
                               borderRadius={25} iconStyle={styles.searchIcon}
                               style={styles.searchIconContainer}
                               onPress={() => this.props.onSearchClick(this.state.query)} />
                </View>
            </Image>

        );
    }
}

const styles = StyleSheet.create({
  imageContainer: {
    height: 170,
    justifyContent: 'flex-end',
    shadowOffset: { height:1, width: 0 }
  },
  container: {
    flexDirection: 'row',
    height: 60,
    backgroundColor: 'rgba(55, 55, 55, 0.9)',
    padding: 5
  },
  searchInput: {
    flex: 1,
    fontSize: 20,
    color: '#ededed'
  },
  searchIconContainer: {
    height: 50,
    width: 50,
    justifyContent: 'center'
  },
  searchIcon: {
    marginLeft:14,
    width: 25
  }
});
